﻿using System;
using System.IO;
using Newtonsoft.Json;




namespace TimeTrees
{
    class Program
    {
        const byte timelineIndexOfDate = 0;
        const byte timelineIndexOfEvent = 1;
        const byte peopleIndexOfNumber = 0;
        const byte peopleIndexOfName = 1;
        const byte peopleIndexOfBorn = 2;
        const byte peopleIndexOfDeath = 3;

        public struct Timeline
        {
            public DateTime date;
            public string Event;
        }


        public struct People
        {
            public int id;
            public string name;
            public DateTime born;
            public DateTime death;
        }


        static void Main(string[] args)
        {

            Console.Write("Введите формат файла файла: ");
            string fileFormat = Console.ReadLine();
            string[] timelimeData = File.ReadAllLines(@"..\..\..\..\timelime." + fileFormat);
            Timeline[] TimelineEvents = new Timeline[timelimeData.Length];
            if (fileFormat == "json")
            {
                Timeline oneEvent;
                for (byte el = 0; el < timelimeData.Length; el++)
                {
                    oneEvent = JsonConvert.DeserializeObject<Timeline>(timelimeData[el]);
                    TimelineEvents[el] = oneEvent;
                }
            }
            else if (fileFormat == "csv")
            {
                TimelineEvents = ReadTimline(timelimeData);
            }
            (var year, var month, var day) = DelDate(TimelineEvents);
            Console.WriteLine($"Между событиями прошло {year} лет {month} месяцев {day} дней ");
            WriteTimelineOnJSON(TimelineEvents);



            string[] peopleData = File.ReadAllLines(@"..\..\..\..\people." + fileFormat);
            People[] Persons = new People[peopleData.Length];
            if (fileFormat == "json")
            {
                People man;
                for (byte el = 0; el < peopleData.Length; el++)
                {
                    man = JsonConvert.DeserializeObject<People>(peopleData[el]);
                    Persons[el] = man;
                }
            }
            else if (fileFormat == "csv")
            {
                Persons = ReadPeople(peopleData);
            }
            foreach (People man in Persons)
                if (LeapYear(man) && Younger20(man))
                    Console.WriteLine(man.name + " родился в високосный год и младше 20");
            WritePeopleOnJSON(Persons);
        } 



        static void WriteTimelineOnJSON(Timeline[] array)
        {
            string[] textToWrite = new string[array.Length];
            for (byte el = 0; el < array.Length; el++)
               textToWrite[el] = JsonConvert.SerializeObject(array[el]);
            File.WriteAllLines(@"..\..\..\..\timelime.json", textToWrite);
        }
        static void WritePeopleOnJSON(People[] array)
        {
            string[] textToWrite = new string[array.Length];
           for (byte el = 0; el < array.Length; el++)
                textToWrite[el] = JsonConvert.SerializeObject(array[el]);
          File.WriteAllLines(@"..\..\..\..\people.json", textToWrite);
        }


        static Timeline[] ReadTimline(string[] timelimeData)
        {
            string[][] splitData = SplitData(timelimeData);
            Timeline[] timeline = new Timeline[timelimeData.Length];
            Timeline oneEvent;
            for (byte el = 0; el < splitData.Length; el++)
            {
                oneEvent.date = ParseDate(splitData[el][timelineIndexOfDate]);
                oneEvent.Event = splitData[el][timelineIndexOfEvent];
                timeline[el] = oneEvent;
            }
            return timeline;
        }


        static People[] ReadPeople(string[] peopleData)
        {
            string[][] splitData = SplitData(peopleData);
            People[] person = new People[peopleData.Length];
            People man;
            for (byte el = 0; el < splitData.Length; el++)
            {
                man.id = int.Parse(splitData[el][peopleIndexOfNumber]);
                man.name = splitData[el][peopleIndexOfName];
                man.born = ParseDate(splitData[el][peopleIndexOfBorn]);
                if (splitData[el].Length == 4)
                {
                    man.death = ParseDate(splitData[el][peopleIndexOfDeath]);
                }
                else
                {
                    man.death = DateTime.MinValue;
                }
                person[el] = man;
            }
            return person;
        }


        static string[][] SplitData(string[] data)
        {
            string[][] split = new string[data.Length][];
            for (short i = 0; i < data.Length; i++)
            {
                split[i] = data[i].Split(';');
            }
            return split;
        }


        static (int, int, int) DelDate(Timeline[] timeline)
        {
            var resDate = DateTime.MinValue;
            (var min, var max) = MinAndMaxDate(timeline);
            TimeSpan diff = max - min;
            resDate += diff;
            return (resDate.Year - 1, resDate.Month - 1, resDate.Day - 1);
        }


        static (DateTime, DateTime) MinAndMaxDate(Timeline[] timeline)
        {
            var minDate = DateTime.MaxValue;
            var maxDate = DateTime.MinValue;
            foreach (Timeline Ev in timeline)
            {
                minDate = minDate > Ev.date ? Ev.date : minDate;
                maxDate = maxDate < Ev.date ? Ev.date : maxDate;
            }
            return (minDate, maxDate);
        }


        


        static bool Younger20(People person)
        {
            var nowDate = DateTime.Now;
            if (person.death != DateTime.MinValue)
            {
                var delDate = person.death.Year - person.born.Year - 1 + ((person.death.Month > person.born.Month || person.death.Month == person.born.Month && person.death.Day >= person.born.Day) ? 1 : 0);
                return (delDate < 20) ? true : false;
            }

            else
            {
                var delDate = nowDate.Year - person.born.Year - 1 + ((nowDate.Month > person.born.Month || nowDate.Month == person.born.Month && nowDate.Day >= person.born.Day) ? 1 : 0);
                return (delDate < 20) ? true : false;
            }
        }


        static bool LeapYear(People person)
        {
            return (person.born.Year % 400 == 0) || (person.born.Year % 4 == 0 && (person.born.Year % 100 != 0)) ? true : false;
        }

        static DateTime ParseDate(object stDate)
        {
            var parseString = (string)stDate;
            if (parseString.Length == 4) return DateTime.ParseExact(parseString, "yyyy", null);
            else if (parseString.Length == 7) return DateTime.ParseExact(parseString, "yyyy-MM", null);
            else return DateTime.ParseExact(parseString, "yyyy-MM-dd", null);
        }

    }
}